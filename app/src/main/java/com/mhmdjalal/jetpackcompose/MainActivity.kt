package com.mhmdjalal.jetpackcompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DefaultPreview()
        }
    }
}

@Composable
fun Header() {
    Surface(color = colorResource(id = R.color.background_resource)) {
        val textState = remember { mutableStateOf(TextFieldValue()) }
        val focusManager = LocalFocusManager.current

        Column(
            Modifier
                .padding(top = 20.dp)
                .fillMaxWidth()
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_add),
                contentDescription = null,
                modifier = Modifier.padding(start = 15.dp),
                tint = colorResource(id = R.color.blue)
            )
            Row(
                modifier = Modifier
                    .padding(top = 10.dp, start = 15.dp, end = 15.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = "My posts",
                    fontSize = 34.sp,
                    fontWeight = FontWeight.Bold,
                )
                Image(
                    painter = painterResource(id = R.mipmap.user),
                    contentDescription = null,
                    modifier = Modifier
                        .width(34.dp)
                        .height(34.dp)
                        .align(CenterVertically),
                    alignment = Alignment.CenterEnd
                )
            }
//            TextField(
//                value = textState.value,
//                onValueChange = { textState.value = it },
//                modifier = Modifier
//                    .padding(top = 10.dp, start = 15.dp, end = 15.dp)
//                    .fillMaxWidth()
//                    .height(40.dp)
//                    .padding(top = 0.dp, bottom = 0.dp),
//                placeholder = {
//                    Text(
//                        modifier = Modifier
//                            .padding(top = 0.dp, bottom = 0.dp)
//                            .fillMaxWidth(),
//                        text = "Search",
//                        style = TextStyle(
//                            color = colorResource(id = R.color.text_field),
//                            fontSize = 17.sp,
//                            background = Color.Magenta
//                        ),
//                        overflow = TextOverflow.Ellipsis,
//                        maxLines = 1
//                    )
//                },
//                colors = TextFieldDefaults.textFieldColors(
//                    focusedIndicatorColor = Color.Transparent,
//                    unfocusedIndicatorColor = Color.Transparent,
//                    disabledIndicatorColor = Color.Transparent,
//                    backgroundColor = colorResource(id = R.color.background_text_field)
//                ),
////                leadingIcon = {
////                    Icon(
////                        painter = painterResource(id = R.drawable.ic_search),
////                        contentDescription = null,
////                        Modifier.background(Color.Magenta)
////                    )
////                },
//                shape = MaterialTheme.shapes.small.copy(CornerSize(10.dp)),
//                singleLine = true,
//                textStyle = TextStyle(
//                    fontSize = 17.sp,
//                ),
//                keyboardActions = KeyboardActions(onSearch = {
//                    focusManager.clearFocus()
//                }),
//                keyboardOptions = KeyboardOptions.Default.copy(
//                    imeAction = ImeAction.Search,
//                    keyboardType = KeyboardType.Text,
//                    autoCorrect = true
//                )
//            )
            TextField(
                value = textState.value,
                onValueChange = { textState.value = it },
                placeholder = {
                    Text(
                        text = "Search",
                        style = TextStyle(
                            color = colorResource(id = R.color.text_field),
                            fontSize = 17.sp,
//                            background = Color.Magenta
                        ),
                        overflow = TextOverflow.Ellipsis,
                        maxLines = 1,
                        modifier = Modifier
                            .padding(top = 0.dp, bottom = 0.dp)
                            .fillMaxWidth()
                            .weight(1f, fill = false),
                    )
                },
                colors = TextFieldDefaults.textFieldColors(
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                    disabledIndicatorColor = Color.Transparent,
                    backgroundColor = colorResource(id = R.color.background_text_field)
                ),
                leadingIcon = {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_search),
                        contentDescription = null
                    )
                },
                shape = MaterialTheme.shapes.small.copy(CornerSize(10.dp)),
                singleLine = true,
                textStyle = TextStyle(
                    fontSize = 17.sp
                ),
                modifier = Modifier
                    .padding(top = 10.dp, start = 15.dp, end = 15.dp)
                    .fillMaxWidth()
                    .padding(top = 0.dp, bottom = 0.dp)
                    .absolutePadding(top = 0.dp, bottom = 0.dp),
                keyboardActions = KeyboardActions(onSearch = {
                    focusManager.clearFocus()
                }),
                keyboardOptions = KeyboardOptions.Default.copy(
                    imeAction = ImeAction.Search,
                    keyboardType = KeyboardType.Text,
                    autoCorrect = true
                )
            )
            Divider(
                color = colorResource(id = R.color.background_text_field),
                modifier = Modifier
                    .padding(top = 15.dp)
                    .height(1.dp)
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun HeaderPreview() {
    MaterialTheme {
        Header()
    }
}

@Composable
fun CardSlider(image: Int, like: String, isLike: Boolean = false, title: String) {
    Card(
        modifier = Modifier
            .padding(start = 10.dp, end = 10.dp)
            .width(200.dp)
            .height(160.dp),
        shape = RoundedCornerShape(20.dp),
        elevation = 0.dp
    ) {
        Image(
            painter = painterResource(id = image), contentDescription = null, modifier = Modifier
                .fillMaxHeight()
                .fillMaxWidth()
        )
        ConstraintLayout(modifier = Modifier.padding(15.dp)) {
            val (icon, labelLike, labelTitle) = createRefs()

            Icon(
                painter = painterResource(id = R.drawable.ic_heart_filled),
                contentDescription = null,
                modifier = Modifier
                    .constrainAs(icon) {
                        end.linkTo(parent.end)
                        top.linkTo(parent.top)
                    },
                tint = colorResource(id = if (isLike) R.color.red else R.color.white)
            )
            Text(
                text = like,
                modifier = Modifier
                    .constrainAs(labelLike) {
                        end.linkTo(icon.start)
                        centerVerticallyTo(icon)
                    }
                    .padding(end = 5.dp),
                style = TextStyle(
                    fontSize = 13.sp,
                    color = colorResource(id = R.color.white),
                )
            )
            Text(
                text = title,
                modifier = Modifier
                    .constrainAs(labelTitle) {
                        start.linkTo(parent.start)
                        bottom.linkTo(parent.bottom)
                    },
                style = TextStyle(
                    fontSize = 17.sp,
                    color = colorResource(id = R.color.white),
                    fontWeight = FontWeight.SemiBold
                )
            )
        }
    }
}

@Composable
fun CardContent() {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 15.dp, vertical = 5.dp),
        shape = RoundedCornerShape(15.dp),
        elevation = 0.dp,
        border = BorderStroke(width = 1.dp, color = colorResource(id = R.color.card_border))
    ) {
        Column(
            modifier = Modifier
                .padding(15.dp)
        ) {
            ConstraintLayout(
                modifier = Modifier.fillMaxWidth()
            ) {
                val (avatar, name, btnFollow) = createRefs()
                Image(
                    painter = painterResource(id = R.mipmap.user),
                    contentDescription = null,
                    modifier = Modifier
                        .height(40.dp)
                        .width(40.dp)
                        .constrainAs(avatar) {
                            start.linkTo(parent.start)
                            top.linkTo(parent.top)
                        }
                )
                Column(
                    modifier = Modifier
                        .padding(start = 15.dp)
                        .constrainAs(name) {
                            start.linkTo(avatar.end)
                            centerVerticallyTo(avatar)
                        },
                ) {
                    Text(
                        text = "Cameron Steward",
                        style = TextStyle(
                            fontSize = 15.sp
                        )
                    )
                    Text(
                        text = "2 hours ago",
                        style = TextStyle(
                            fontSize = 12.sp,
                            color = colorResource(id = R.color.text_field)
                        )
                    )
                }
                OutlinedButton(
                    onClick = { },
                    border = BorderStroke(1.dp, colorResource(id = R.color.blue)),
                    shape = RoundedCornerShape(12f),
                    modifier = Modifier
                        .height(34.dp)
                        .constrainAs(btnFollow) {
                            end.linkTo(parent.end)
                            centerVerticallyTo(avatar)
                        }
                ) {
                    Text(
                        text = "Follow",
                        style = TextStyle(
                            color = colorResource(id = R.color.blue),
                            fontSize = 12.sp,
                            fontWeight = FontWeight.SemiBold
                        )
                    )
                }
            }

            Spacer(modifier = Modifier.height(20.dp))
            Text(
                text = "#architecture, #relax",
                style = TextStyle(
                    color = colorResource(id = R.color.blue),
                    fontSize = 12.sp,
                    fontWeight = FontWeight.SemiBold
                )
            )
            Spacer(modifier = Modifier.height(10.dp))
            Text(
                text = "Sed affert delenit ea. Nam at ferri facete inermis. Eum dicta fuisset phaedrum ei, et amet iuvaret vituperatoribus vix.",
                style = TextStyle(
                    fontSize = 15.sp
                )
            )
            Spacer(modifier = Modifier.height(10.dp))
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {
                Image(
                    painter = painterResource(id = R.mipmap.post1),
                    contentDescription = null,
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .size(150.dp)
                        .padding(end = 5.dp)
                        .clip(RoundedCornerShape(15.dp))
                )
                Image(
                    painter = painterResource(id = R.mipmap.post2),
                    contentDescription = null,
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .size(150.dp)
                        .padding(start = 5.dp)
                        .clip(RoundedCornerShape(15.dp))
                )
            }
        }
    }
}

//@Preview(showBackground = true)
//@Composable
//fun CardPreview() {
//    MaterialTheme {
//        CardSlider(R.mipmap.img, "6473", title = "Hot air balloon")
//    }
//}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MaterialTheme {
        val scaffoldState = rememberScaffoldState(rememberDrawerState(DrawerValue.Open))
        val selectedItem = remember { mutableStateOf("home") }

        Scaffold(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            scaffoldState = scaffoldState,
            content = {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .fillMaxHeight()
                ) {
                    Header()

                    LazyColumn {
                        item {
                            Spacer(modifier = Modifier.height(20.dp))
                            ScrollableTabRow(
                                selectedTabIndex = 0,
                                backgroundColor = Color.Transparent,
                                edgePadding = 5.dp,
                                divider = { TabRowDefaults.Divider(thickness = 0.dp) },
                                indicator = {
                                    TabRowDefaults.Indicator(
                                        color = Color.Transparent,
                                        height = 0.dp
                                    )
                                }
                            ) {
                                CardSlider(
                                    R.mipmap.img,
                                    "6473",
                                    isLike = true,
                                    title = "Hot air balloon"
                                )
                                CardSlider(R.mipmap.img2, "7465", title = "Jellyfish")
                                CardSlider(R.mipmap.img, "6473", title = "Hot air balloon")
                            }
                            Spacer(modifier = Modifier.height(20.dp))
                            Text(
                                text = "Other posts",
                                fontSize = 22.sp,
                                fontWeight = FontWeight.Bold,
                                modifier = Modifier
                                    .padding(start = 15.dp)
                            )
                            Spacer(modifier = Modifier.height(15.dp))
                            CardContent()
                            CardContent()
                            CardContent()
                        }
                    }
                }
            },
            bottomBar = {
                data class MenuBottom(val icon: Int, val name: String)

                val arrMenu = arrayOf(
                    MenuBottom(icon = R.drawable.ic_home, name = "Home"),
                    MenuBottom(icon = R.drawable.ic_message, name = "Chat"),
                    MenuBottom(icon = R.drawable.ic_person_2, name = "Friends"),
                    MenuBottom(icon = R.drawable.ic_bookmark, name = "Stories")
                )

                BottomAppBar(
                    backgroundColor = colorResource(id = R.color.background_bottom),
                    contentPadding = PaddingValues(0.dp)
                ) {
                    BottomNavigation(
                        backgroundColor = colorResource(id = R.color.background_bottom),
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        BottomNavigationItem(
                            icon = {
                                Icon(
                                    painter = painterResource(id = arrMenu[0].icon),
                                    contentDescription = null,
                                    tint = colorResource(id = if (selectedItem.value == "home") R.color.blue else R.color.menu_unselected)
                                )
                            },
                            label = {
                                Text(
                                    text = arrMenu[0].name,
                                    style = TextStyle(
                                        fontSize = 10.sp,
                                        color = colorResource(id = if (selectedItem.value == "home") R.color.blue else R.color.menu_unselected),
                                        fontWeight = FontWeight.Medium
                                    )
                                )
                            },
                            selected = selectedItem.value == "home",
                            onClick = {
                                selectedItem.value = "home"
                            },
                            alwaysShowLabel = true
                        )
                        BottomNavigationItem(
                            icon = {
                                Icon(
                                    painter = painterResource(id = arrMenu[1].icon),
                                    contentDescription = null,
                                    tint = colorResource(id = if (selectedItem.value == "chat") R.color.blue else R.color.menu_unselected)
                                )
                            },
                            label = {
                                Text(
                                    text = arrMenu[1].name,
                                    style = TextStyle(
                                        fontSize = 10.sp,
                                        color = colorResource(id = if (selectedItem.value == "chat") R.color.blue else R.color.menu_unselected),
                                        fontWeight = FontWeight.Medium
                                    )
                                )
                            },
                            selected = selectedItem.value == "chat",
                            onClick = {
                                selectedItem.value = "chat"
                            },
                            alwaysShowLabel = true
                        )
                        BottomNavigationItem(
                            icon = {
                                Icon(
                                    painter = painterResource(id = arrMenu[2].icon),
                                    contentDescription = null,
                                    tint = colorResource(id = if (selectedItem.value == "friends") R.color.blue else R.color.menu_unselected)
                                )
                            },
                            label = {
                                Text(
                                    text = arrMenu[2].name,
                                    style = TextStyle(
                                        fontSize = 10.sp,
                                        color = colorResource(id = if (selectedItem.value == "friends") R.color.blue else R.color.menu_unselected),
                                        fontWeight = FontWeight.Medium
                                    )
                                )
                            },
                            selected = selectedItem.value == "friends",
                            onClick = {
                                selectedItem.value = "friends"
                            },
                            alwaysShowLabel = true
                        )
                        BottomNavigationItem(
                            icon = {
                                Icon(
                                    painter = painterResource(id = arrMenu[3].icon),
                                    contentDescription = null,
                                    tint = colorResource(id = if (selectedItem.value == "stories") R.color.blue else R.color.menu_unselected)
                                )
                            },
                            label = {
                                Text(
                                    text = arrMenu[3].name,
                                    style = TextStyle(
                                        fontSize = 10.sp,
                                        color = colorResource(id = if (selectedItem.value == "stories") R.color.blue else R.color.menu_unselected),
                                        fontWeight = FontWeight.Medium
                                    )
                                )
                            },
                            selected = selectedItem.value == "stories",
                            onClick = {
                                selectedItem.value = "stories"
                            },
                            alwaysShowLabel = true
                        )
                    }
                }
            }
        )
    }
}